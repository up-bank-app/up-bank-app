import { useContext, createContext } from 'react'

/**
 * Creates the context with the values and setting functions to be used throughout the application.
 */
export const AppContext = createContext({
  accounts: {
    accountsList: {},
    activeAccount: 'TRANSACTIONAL',
    activeAccountId: ''
  },
  setAccounts: () => {},
  transactions: {},
  setTransactions: () => {},
  selectedMonth: {},
  setSelectedMonth: () => {},
  token: 'null',
  setToken: () => {},
})

export function useAppContext () {
  return useContext(AppContext)
}
