import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "../Containers/NotFound";
import { Home } from "../Pages/Home";
import { CustomSearch } from "../Pages/CustomSearch"

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/transactions">
        <Home />
      </Route>
      <Route exact path="/custom">
        <CustomSearch/>
      </Route>
      <Route>
        <NotFound/>
      </Route>
    </Switch>
  );
}