import axios from 'axios'

export default async function APICall (url, tokens) {
  const baseUrl = 'https://api.up.com.au/api/v1/'
  try {
    const res = await axios.get(baseUrl + url, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        'content-type': 'application/json',
        'Authorization': `Bearer ${tokens}`
      }
    })
    return res
  } catch (err) {
    return 'error'
  }
}