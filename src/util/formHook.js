import { useState } from 'react'

/**
 * Is used for user input sections to set the state of the entered field.
 * @param {*} initialState takes the initial state of our form fields as an object and saves it as a state variable called fields.
 */
export const useFormFields = (initialState) => {
  const [fields, setValues] = useState(initialState)
  return [
    fields,
    e => {
      setValues({
        ...fields,
        [e.target.name]: e.target.value
      })
    }
  ]
}
