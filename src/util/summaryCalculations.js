
import _ from 'lodash';

// eslint-disable-next-line import/no-anonymous-default-export
export default ({
  createSummaryData(data) {
    let moneyIn = 0
    let moneyOut = 0
    let transactionCount = 0
    for (const key in data['data']) {
      let value = data['data'][key].attributes.amount.value
      if (Math.sign(value) !== -1) {
        if (data['data'][key].attributes.description.indexOf("Cover from") === -1 && data['data'][key].attributes.description.indexOf("Transfer from") === -1)
          moneyIn += parseInt(value)
      } else if (data['data'][key].attributes.description.indexOf("Cover from") === -1 && data['data'][key].attributes.description.indexOf("Transfer from") === -1 && data['data'][key].attributes.description.indexOf("Transfer to") === -1 && data['data'][key].attributes.description.indexOf("Quick save transfer") === -1) {
        transactionCount += 1
        value = value.replace("-", "");
        moneyOut += parseInt(value)
      }
    }
    
    const categorisedMonth = data['data'].map((transaction) => {
      const parentCategory = transaction.relationships.parentCategory.data !== null ? transaction.relationships.parentCategory.data.id : null
      return { transaction, parentCategory };
    });
    let selectedMonthData = {
      'categories': {
        'good-life': {'amount': 0, 'transactions': 0}, 
        'personal': {'amount': 0, 'transactions': 0}, 
        'home': {'amount': 0, 'transactions': 0}, 
        'transport': {'amount': 0, 'transactions': 0}
      }
    }
    let test = {}
    test['categories'] = _.groupBy(categorisedMonth, 'parentCategory')

    delete test['categories']['null']
    for (const key in test['categories']) {
      let value = 0
      let transactions = 0
      for (let index = 0; index < test['categories'][key].length; index++) {
        if (Math.sign(test['categories'][key][index].transaction.attributes.amount.value) === -1) {
          value += Math.abs(test['categories'][key][index].transaction.attributes.amount.value)

        } else {
          value -= Math.abs(test['categories'][key][index].transaction.attributes.amount.value)
        }
        transactions++
      }
      selectedMonthData['categories'][key]['amount'] = value.toFixed(2)
      selectedMonthData['categories'][key]['transactions'] = transactions
    }
    selectedMonthData['transactions'] = transactionCount
    selectedMonthData['moneyIn'] = moneyIn
    selectedMonthData['moneyOut'] = moneyOut
    return selectedMonthData
  }
})