import './App.scss';
import React, { useState, useEffect } from 'react'
import { AppContext } from './util/context'
import 'bootstrap/dist/css/bootstrap.min.css';
import Auth from './Pages/Auth'
import NavBar from './Containers/NavBar';
import Routes from "./util/Routes";
import APICall from './util/APICall';

function App() {
  const [accounts, setAccounts] = useState({
    accountsList: {},
    activeAccount: 'TRANSACTIONAL',
    activeAccountId: ''
  });
  const [transactions, setTransactions] = useState(null);
  const [activeAccount, setActiveAccount] = useState(null);
  const [activeAccountId, setActiveAccountId] = useState(null);
  const [selectedMonth, setSelectedMonth] = useState(null);
  const [token, setToken] = useState(null);

  const authToken = async (fieldToken) => {
    const apiResponse = await APICall('util/ping', fieldToken)
    if (apiResponse === 'error') {
      console.log('no')
    } else {
      setToken(fieldToken)
    }
  }
  
  useEffect(() => {
    const getAccounts = async () => {
      const apiResponse = await APICall('accounts', token)
      setAccounts(currentState => ({
        ...currentState,
        accountsList: apiResponse.data.data
      }))
    }
      if (token !== null) {
        getAccounts()
      }

  }, [setAccounts, setToken, token])

  return (
    <div className='app'>
      {token !== null
        ? <AppContext.Provider value={{ accounts, setAccounts, transactions, setTransactions, activeAccount, setActiveAccount, selectedMonth, setSelectedMonth, activeAccountId, setActiveAccountId, token, setToken }}>
          <NavBar />
          <Routes />
        </AppContext.Provider>
        : <Auth functions={authToken} />
      }
    </div>
  );
}

export default App;
