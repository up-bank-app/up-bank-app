import React, { useState, useEffect } from 'react'
import { Graph } from '../Components/Graph'
import { Summary } from '../Components/Summary'
import Transactionss from './Transactions'
import { useAppContext } from '../util/context'
import moment from 'moment';
import APICall from '../util/APICall'

export default function AccountSummary() {
  const { token, accounts } = useAppContext()
  const [currentSummary, setCurrentSummary] = useState({ data: [] });

  useEffect(() => {
    if (accounts.activeAccountId) {
      loadData()
    }
  }, [accounts.activeAccountId])

  const loadData = async (nextPage, data) => {
    const startDate = moment().clone().format("YYYY-MM")
    const endDate = moment().clone().endOf('month').format("YYYY-MM-DD")
    let url = !nextPage ? `accounts/${accounts.activeAccountId}/transactions?filter[since]=${startDate}-01T01:02:03%2B10:00&filter[until]=${endDate}T01:02:03%2B10:00&page[size]=100` : nextPage
    const apiResponse = await APICall(url, token)
    let updatedData = {
      'data': [],
      'links': []
    }
    if (data === undefined) {
      updatedData['data'] = apiResponse['data']['data']
    } else {
      updatedData['data'] = data['data'].concat(apiResponse['data']['data'])
    }
    if (apiResponse['data']['links']['next'] !== null) {
      let page = apiResponse['data']['links']['next'].split('https://api.up.com.au/api/v1/')
      loadData(page[1], updatedData)
    } else {
      setCurrentSummary(updatedData)
    }
  }

  return (
      <div className='content'>
        {accounts.activeAccount !== 'SAVER' &&
          <>
            {currentSummary.data.length !== 0 &&
              <div className='summaryContainer'>
                <h2>{moment().clone().format("MMMM YYYY")} Summary</h2>
                <Graph data={currentSummary} month={moment().clone().format("MMMM YYYY")} />
                <Summary data={currentSummary} />
              </div>
            }
          </>
        }
        <Transactionss />
      </div>
  )
}