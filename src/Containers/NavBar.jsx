import React, { useCallback } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { useAppContext } from '../util/context'
import { LinkContainer } from "react-router-bootstrap";
import { Button } from '../Components/Button'
import './styles/NavBar.scss'
export default function NavBar() {
  const { setToken } = useAppContext()
  const logout = useCallback(() => {
    setToken(null)
  }, [setToken])
  return (
    <Navbar fixed="top" bg="light" expand="lg">
      <LinkContainer to="/">
        <Navbar.Brand>Up Bank API</Navbar.Brand>
      </LinkContainer>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <LinkContainer to="/transactions">
            <Nav.Link>Transactions</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/custom">
            <Nav.Link>Custom</Nav.Link>
          </LinkContainer>
        </Nav>
        <Button onClickFunction={() => logout()} text='Logout' size='small' />
      </Navbar.Collapse>
    </Navbar>
  )
}
