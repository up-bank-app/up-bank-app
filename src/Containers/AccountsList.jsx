import React, { useState, useEffect, useCallback } from 'react'
import { useAppContext } from '../util/context'
import { AccountButtons } from '../Components/AccountButtons'
import APICall from '../util/APICall'

export const AccountsList = ({ accounts }) => {
  const { setAccounts, setTransactions, token } = useAppContext()
  const [activeClass, setActiveClass] = useState('')

  useEffect(() => {
    if (accounts !== null) {
      getDetails(accounts[0].id, accounts[0].attributes.accountType)
    }
  }, [accounts])

  const getDetails = useCallback(async (id, type) => {
    const apiResponse = await APICall(`accounts/${id}/transactions?page[size]=50`, token)
    setAccounts(currentState => ({
      ...currentState,
      activeAccount: type,
      activeAccountId: id
    }))
    setTransactions(apiResponse.data)
    setActiveClass(id)
  }, [])

  const isActive = (id) => id === activeClass ? 'active' : ''

  return (
    <>
      <h2>Accounts</h2>
      {accounts !== {} &&
        <>
          {accounts.map((account, keys) =>
            <AccountButtons key={keys} account={account} keys={keys} isActive={isActive} getDetails={getDetails} />
          )}
        </>
      }
    </>
  )
}