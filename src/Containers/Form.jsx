import React, { useState } from 'react'
import DatePicker from "react-datepicker";
import moment from 'moment';
import { Button } from '../Components/Button'
import "react-datepicker/dist/react-datepicker.css";
import './styles/Form.scss'
export const Form = ({getData}) => {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  
  return (
    <div className='formContainer'>
        <div className="row">
          <div className="inputDiv">
            <label>Transactions</label>
          </div>
          <div className="inputDiv">
            <input disabled type="text" value="Transaction Account" />
          </div>
        </div>
        <div className="row">
          <div className="inputDiv">
            <label>Start Date</label>
          </div>
          <div className="inputDiv">
          <DatePicker dateFormat="dd/MM/yyyy" className='datePicker' selected={startDate} onChange={date => setStartDate(date)} />
          </div>
        </div>
        <div className="row">
          <div className="inputDiv">
            <label>End Date</label>
          </div>
          <div className="inputDiv">
          <DatePicker dateFormat="dd/MM/yyyy" className='datePicker' selected={endDate} onChange={date => setEndDate(date)} />
          </div>
        </div><br/>
        <div className="row">
        <Button onClickFunction={() => getData(moment(startDate).format("YYYY-MM-DD"), moment(endDate).format("YYYY-MM-DD"))} size='small' text='Search'/>
        </div>
    </div>
  )
}


