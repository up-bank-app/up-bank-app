import React, { useState, useEffect } from 'react'
import moment from 'moment';
import { useAppContext } from '../util/context'
import { Graph } from '../Components/Graph'
import { Summary } from '../Components/Summary'
import TransactionTable from '../Components/TransactionTable'
import {LoadMore} from '../Components/LoadMore'
import _ from 'lodash';
import APICall from '../util/APICall';

export default function Transactions() {
  const { transactions, token, accounts } = useAppContext()
  const [transactionsByDate, setTransactionsByDate] = useState();
  const [showSummary, setShowSummary] = useState();
  const [MonthSummary, setMonthSummary] = useState({ data: [] });
  const [nextPage, setNextPage] = useState();
  const [monthGroups, setMonthGroups] = useState([])

  useEffect(() => {
    const transactionsWithDates = transactions['data'].map((transaction) => {
      const createdAt = moment(transaction.attributes.createdAt).local();
      const timeString = createdAt.format('h:mma');
      const dateString = createdAt.format('ddd, D MMMM yyyy');
      const monthYearString = createdAt.format('MMMM yyyy')
      return { transaction, timeString, dateString, monthYearString };
    });
    transactions['links']['next'] !== null ? setNextPage(transactions['links']['next'].substring(38, transactions['links']['next'].length)) : setNextPage(null)
    setMonthGroups(Object.entries(_.groupBy(transactionsWithDates, 'monthYearString')))
  }, [transactions])

  useEffect(() => {
    for (let index = 0; index < monthGroups.length; index++) {
      monthGroups[index][1] = Object.entries(_.groupBy(monthGroups[index][1], 'dateString'))
    }
    setTransactionsByDate(monthGroups)
  }, [monthGroups, setMonthGroups])

  const loadSummary = async (month) => {
    month === showSummary ? setShowSummary('') : await loadSummaryData(moment(month).format("YYYY-MM"), moment(month).endOf('month').format("YYYY-MM-DD"), undefined, undefined, month)
  }

  // TO DO: Add account id here as param
  const loadSummaryData = async (startDate, endDate, nextPage, data, month) => {
    let url = !nextPage ? `accounts/${accounts.activeAccountId}/transactions?filter[since]=${startDate}-01T00:00:01%2B10:00&filter[until]=${endDate}T23:59:59%2B10:00&page[size]=100` : nextPage
    const apiResponse = await APICall(url, token)
    let updatedData = {
      'data': [],
      'links': []
    }
    updatedData['data'] = !data ? apiResponse['data']['data'] : data['data'].concat(apiResponse['data']['data'])
    if (apiResponse['data']['links']['next']) {
      let page = apiResponse['data']['links']['next'].split('https://api.up.com.au/api/v1/')
      loadSummaryData(startDate, endDate, page[1], updatedData, month)
    } else {
      setMonthSummary(updatedData)
      setShowSummary(month)
    }
  }

  return (
    <div>
      <h2>Transactions</h2>
      {transactionsByDate && transactionsByDate.length > 0 &&
        <>
          {transactionsByDate.map((month, key) =>
            <React.Fragment key={key}>
              {showSummary === month[0] &&
                <React.Fragment key={key}>
                  <Graph data={MonthSummary} month={month[0]} />
                  <Summary data={MonthSummary} />
                </React.Fragment>
              }
              <TransactionTable accountType={accounts.activeAccount} month={month} keys={key} function={loadSummary} />
            </React.Fragment>
          )}
          {nextPage !== null &&
            <LoadMore nextPage={nextPage} />
          }
        </>
      }
    </div>
  )
}