import React, { useState, useEffect } from 'react'
import { Form } from '../Containers/Form'
import { useAppContext } from '../util/context'
import Table from 'react-bootstrap/Table'
import './styles/CustomSearch.scss'
import { Graph } from '../Components/Graph'
import { Summary } from '../Components/Summary'
import TransactionEntry from '../Components/TransactionEntry'

import moment from 'moment';
import _ from 'lodash';
import APICall from '../util/APICall'


export const CustomSearch = () => {
  const { token, accounts } = useAppContext()
  const [state, setDate] = useState()
  const [data, setData] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState()
  useEffect(() => {
    if (state) {
      const transactionsWithDates = state.data.map((transaction) => {
        const createdAt = moment(transaction.attributes.createdAt).utc();
        const timeString = createdAt.format('h:mma');
        const dateString = createdAt.format('ddd, D MMMM yyyy');
        const monthYearString = createdAt.format('MMMM yyyy')
        return { transaction, timeString, dateString, monthYearString };
      });
      setData(Object.entries(_.groupBy(transactionsWithDates, 'dateString')))
    }

  }, [state])

  const getData = async (startDate, endDate, nextPage, data, month) => {
    let url = !nextPage ? `accounts/${accounts.accountsList[0].id}/transactions?filter[since]=${startDate}T00:00:01%2B10:00&filter[until]=${endDate}T23:59:59%2B10:00&page[size]=100` : nextPage
    const apiResponse = await APICall(url, token)
    let updatedData = {
      'data': [],
      'links': []
    }
    updatedData['data'] = !data ? apiResponse['data']['data'] : data['data'].concat(apiResponse['data']['data'])
    if (apiResponse['data']['links']['next']) {
      let page = apiResponse['data']['links']['next'].split('https://api.up.com.au/api/v1/')
      getData(startDate, endDate, page[1], updatedData, month)
    } else {
      setStartDate(moment(startDate).format("DD-MM-YYYY"))
      setEndDate(moment(endDate).format("DD-MM-YYYY"))
      setDate(updatedData)
    }
  }

  return (
    <div className='mainContainer'>
      <div className='sideUtil'>
        <Form getData={getData} />
      </div>
      <div className='content'>
        {state &&
          <>
            <Graph data={state} month={startDate + ' - ' + endDate} />
            <Summary data={state} />
            <h2>Transactions</h2>
            <Table hover>
              <thead className='tableHeader' key={'tableHeader-'}>
                <tr>
                  <td colSpan="3"><h4>{startDate + ' - ' + endDate}</h4></td>
                </tr>
              </thead>
              <tbody>
                {data &&
                  <>
                    {data.map(([dateString, records], index) => (
                      <React.Fragment key={index}>
                        <tr key={index}>
                          <td key={index} className='date' colSpan="3">{dateString}</td>
                        </tr>
                        {records.map((record, index) => (
                          <TransactionEntry accountType={'TRANSACTIONAL'} key={index} index={index} record={record.transaction} timeString={record.timeString} />
                        ))}
                      </React.Fragment>
                    ))}
                  </>
                }
              </tbody>
            </Table>
          </>
        }
      </div>
    </div>
  )
}
