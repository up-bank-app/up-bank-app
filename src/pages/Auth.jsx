import React from 'react'
import { useFormFields } from '../util/formHook'
import { Button } from '../Components/Button'
import './styles/auth.scss'
export default function Auth(props) {
  const [fields, handleChange] = useFormFields({
    token: ''
  })
  return (
    <div className='mainContainer'>
      <h1>Enter your up bank api</h1>
      <div className='authForm'>
        <input placeholder='API Key' className='tokenInput' name='token' type='username' value={fields.token} onChange={handleChange}/>
        <Button onClickFunction={() => props.functions(fields.token)} size='normal' text='Login'/>
      </div>
    </div>
  )
}
