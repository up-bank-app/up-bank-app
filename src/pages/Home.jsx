import React from 'react'
import { AccountsList } from '../Containers/AccountsList'
import AccountSummary from '../Containers/AccountSummary'
import { useAppContext } from '../util/context'

export const Home = () => {
  const { transactions, accounts } = useAppContext()

    return (
    <div className='mainContainer'>
      <>
        <div className='sideUtil'>
          {accounts.accountsList.length !== 0 && accounts.accountsList.length !== undefined &&
            < AccountsList accounts={accounts.accountsList} />
          }
        </div>
        {transactions !== null &&
          <AccountSummary />
        }
      </>
    </div>
  )
}