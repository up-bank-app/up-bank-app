import React from 'react'
import Icons from './icons'
import './styles/CategoryTiles.scss'
export const CategoryTiles = ({ selectedMonth }) => {
  return (
    <div className='categories'>
      {Object.keys(selectedMonth['categories']).map((category, index) => (
        <div key={index} className='categoryIconsDiv'>
          <span key={'span-' + index} className='categoryIcons'><Icons name={category} /></span>
          <h5 key={'h5-' + index} className={category}>{category[0].toUpperCase() + (category.replace('-', ' ')).slice(1)}</h5>
          <p key={'p-' + index} className={category}>Transactions: {selectedMonth['categories'][category]['transactions']}</p><span>${selectedMonth['categories'][category]['amount']}</span>
        </div>
      ))}
    </div>
  )
}
