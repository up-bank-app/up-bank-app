import React from 'react'
import './styles/AccountButtons.scss'
export const AccountButtons = ({ account, keys, isActive, getDetails }) => {
  return (
    <div key={keys} className={'accountsButton ' + isActive(account.id)} onClick={() => getDetails(account.id, account.attributes.accountType)}>
      <span className='displayName'>
        {account.attributes.displayName}
      </span>
      <span className='accountBalance'>
        ${Number(account.attributes.balance.value).toLocaleString('en')}
      </span>
    </div>
  )
}
