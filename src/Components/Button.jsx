import React from 'react'
import './styles/Button.scss'
export const Button = ({onClickFunction, text, size}) => {

  const buttonSize = size => size === 'small' ? 'buttonSmall' : 'buttonNormal'

  return <button className={buttonSize(size) + ' button'} onClick={onClickFunction}>{text}</button>
}
