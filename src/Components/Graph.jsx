import React from 'react'
import { Bar } from 'react-chartjs-2';
import createSummaryData from '../util/summaryCalculations'
import './styles/Graph.scss'

export const Graph = ({data, month}) => {
  let datasets = createSummaryData.createSummaryData(data)
  let graphData = {
    labels: ['Good Life', 'Personal', 'Home', 'Transport'],
    datasets: [{
      data: [datasets['categories']['good-life']['amount'], datasets['categories']['personal']['amount'], datasets['categories']['home']['amount'], datasets['categories']['transport']['amount']],
      color: "#f1f1f3",
      backgroundColor: [
        '#f9db42',
        '#fd8939',
        '#c775c2',
        '#5693cd'
      ]
    }]
  }

  let options = {
    scales: {
      xAxes: [{
        gridLines: {
          display: false
        },
        ticks: {
          fontColor: "#f1f1f3"
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          fontColor: "#f1f1f3"
        }
      }]
    },
    legend: {
      display: false
    },
    title: {
      display: true,
      text: `Spending during ${month}`,
      fontSize: 20,
      fontColor: "#f1f1f3"
    },
    maintainAspectRatio: false
  }

  return (
    <div className='graph'>
      <div className='graphContents'>
        <Bar
          backgroundColor={'#fbfbfb'}
          data={graphData}
          options={options}
        />
      </div>
    </div>
  )
}