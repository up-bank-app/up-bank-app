import React from 'react'
import createSummaryData from '../util/summaryCalculations'
import './styles/Summary.scss'
import { CategoryTiles } from './CategoryTiles'

export const Summary = ({data}) => {
  let selectedMonth = createSummaryData.createSummaryData(data)

  return (
    <div className='summary'>
      <div className='summaryContents'>
        <div className='money'>
          <div className='moneyOut'><h4>${selectedMonth.moneyOut.toFixed(2)}</h4><p>Money Out</p> </div>
          <div className='moneyIn'><h4>+${selectedMonth.moneyIn.toFixed(2)}</h4><p>Money In</p></div>
          <div className='transactionsAmount'><h4>{selectedMonth.transactions}</h4><p>Transactions</p></div>
        </div>
        <CategoryTiles selectedMonth={selectedMonth}/>
      </div>
    </div>
  )
}