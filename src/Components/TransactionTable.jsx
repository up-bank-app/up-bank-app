import React from 'react'
import Table from 'react-bootstrap/Table'
import TransactionEntry from './TransactionEntry'
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import './styles/TransactionTable.scss'

export default function TransactionTable(props) {
  return (
    <Table hover key={props.keys}>
      <thead className='tableHeader' key={'tableHeader-' + props.keys}>
        <tr key={props.keys}>
          {props.month[0] === moment().clone().format("MMMM YYYY") || props.accountType === 'SAVER'
            ? <td key={props.keys} colSpan="3"><h4 key={props.keys}>{props.month[0]}</h4></td>
            : <td key={props.keys} colSpan="3" onClick={() => props.function(props.month[0])}><h4 className='loadSummary'  key={props.keys}>{props.month[0]}</h4><FontAwesomeIcon className='arrowIcon' icon={faChevronRight} size="lg" /></td>
          }
        </tr>
      </thead>
      <tbody key={'tbody' + props.keys}>
        {props.month[1].map(([dateString, records], index) => (
          <React.Fragment key={index}>
            <tr key={index}>
              <td key={index} className='date' colSpan="3">{dateString}</td>
            </tr>
            {records.map((record, index) => (
              <TransactionEntry accountType={props.accountType} key={index} index={index} record={record.transaction} timeString={record.timeString}/>
            ))}
          </React.Fragment>
        ))}
      </tbody>
    </Table>
  )
}