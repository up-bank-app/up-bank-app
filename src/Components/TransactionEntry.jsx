import React, { useState, useEffect } from 'react'
import Icons from './icons'
import Avatar from 'react-avatar';
import './styles/TransactionEntry.scss'


export default function TransactionEntry({ accountType, index, record, timeString }) {
  const [isOpen, setIsOpen] = useState('closed')

  useEffect(() => {
    setIsOpen('closed')
  }, [])

  const setIconName = (name, index) => {
    return !name.data ? '' : <Icons key={'icon-' + index} name={name.data.id} />
  }

  const displayAmount = (value) =>
    Math.sign(value) !== -1 ? '+$' + Number(value).toLocaleString('en') : '$' + Number(value.replace("-", "")).toLocaleString('en')

  const displayColor = (value) => Math.sign(value) !== -1 ? 'income' : ''

  const message = (message, rawText, timeString) =>
    message === null && rawText !== null ? timeString + ', ' + rawText
      : rawText === null && message !== null ? timeString + ', ' + message
        : timeString + ''


  const loadTransaction = () => {
    isOpen === 'open' ? setIsOpen('closed') : setIsOpen('open')
  }

  const roundUpTotal = (roundUp, total) => '$' + (parseFloat(Number(roundUp.replace("-", "")).toLocaleString('en')) + parseFloat(Number(total.replace("-", "")).toLocaleString('en')))


  const roundUp = () => {
    if (isOpen === 'open' && record.attributes.roundUp) {
      return <div className='roundUp'>
        <span>Round-up </span>
        <div className='amounts'>
          <div><span> {displayAmount(record.attributes.roundUp.amount.value)}</span>
            <span className='message'>Amount</span>
          </div>
          <div><span> {roundUpTotal(record.attributes.roundUp.amount.value, record.attributes.amount.value)}</span>
            <span className='message'>Total</span>
          </div>
        </div>
      </div>
    }
  }

  const category = () => {
    if (isOpen === 'open' && record.relationships.category.data) {
      return <div className='balance'><span className={record.relationships.parentCategory.data.id}>{record.relationships.category.data.id}</span></div>
    }
  }

  const status = () => {
    if (isOpen === 'open' && record.attributes.status === 'HELD') {
      return <div className='status'><span>PENDING</span></div>
    }
  }

  return (
    <React.Fragment key={index}>
      <tr onClick={accountType !== 'SAVER' ? () => loadTransaction() : null} className={isOpen + " transactionEntry"} key={index} >
        <td className='avatar' key={index + '0'}>
          <Avatar key={index} textSizeRatio={2.5} size={40} round={true} name={record.attributes.description} />
        </td>
        <td className='mainArea' key={index + '1'}>
          <span key={'description-' + index}>{record.attributes.description}</span>
          {setIconName(record.relationships.category, index)}
          <span key={'message-' + index} className='message'>{message(record.attributes.message, record.attributes.rawText, timeString)}</span>
          {roundUp()}
          {category()}
        </td>
        <td className='balance' key={index + '3'}>
          <span className={displayColor(record.attributes.amount.value) + ' balance'}>{displayAmount(record.attributes.amount.value)}</span>
          {status()}
        </td>
      </tr>
    </React.Fragment>
  )
}