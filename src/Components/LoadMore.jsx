import React from 'react'
import { useAppContext } from '../util/context'
import { Button } from './Button'
import APICall from '../util/APICall'

export const LoadMore = ({nextPage}) => {
  const { transactions, setTransactions, token } = useAppContext()
  const loadMore = async (nextPage) => {
    const apiResponse = await APICall(`accounts/${nextPage}`, token)
    setTransactions({data: [...transactions.data, ...apiResponse.data.data], links: apiResponse.data.links})
  }
  return <Button size='normal' text='Load More' onClickFunction={() => loadMore(nextPage)} />
}
